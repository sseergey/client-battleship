#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>

enum global_consts_t {
    PORT = 2000
};

enum client_states {
    CLOSED = -1,
    HANDSHAKE = 0,
    ACQ,
    ACQ_CONFIRM,
    CHOOSE_RIVAL,
    CHOOSE_RIVAL_GET_LIST,
    CHOOSE_RIVAL_GET_NAMES,
    CHOOSE_RIVAL_CONFIRM,
    WAIT_GAME_OFFER,
    WAIT_GAME_STATUS,
    PLACE_SHIPS,
    GET_FIELD,
    GAME_START,
    GAME_ACTION,

};

bool assembleString(char* recv_msg, char* data, char* buf);

int client(int state, int fd, char* recv_msg);
int handshake(int fd, char* recv_msg);
int acq(int fd, char* name, char* recv_msg);
int acqConfirm(char* recv_msg);
int chooseRivalGetList(char* recv_msg);
int chooseRivalGetNames(char* recv_msg);
int chooseRival(int fd, char* enemy_name, char* recv_msg);
int chooseRivalConfirm(char* recv_msg);
int waitGameOffer(int fd, char* game_start, char* recv_msg);
int waitGameStatus(char* recv_msg);
int placeShips(int fd, char* recv_msg);
int gameStart(char* recv_msg);
int gameAction(int fd, char* recv_msg);
int getField(int fd, char* recv_msg);

static char name[80];
static char enemy_name[80];
static char game_start[80];
static int clients_cnt = 0;
static int ships_cnt = 10;
static int pos = 0;
static bool flag = true;

int main() {
    // TODO: выключаться при потере соединения, а не спамить стрелками
    // TODO: парсить входящие сообщения по переводу строки.
    // TODO: начать выполнять проверки внутренности приходящих сообщений
    int result;
    int fd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;


    result = bind(fd, (struct sockaddr*)&addr, sizeof(addr));
    if (result != 0) {
        return -1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);

    result = inet_aton("127.0.0.1", &addr.sin_addr);
    if (result == 0) {
        return -1;
    }
    connect(fd, (struct sockaddr *)&addr, sizeof(addr));

    printf("My name =");
    scanf("%s", name);

    printf("Enemy name =");
    scanf("%s", enemy_name);

    printf("Game start =");
    scanf("%s", game_start);

    printf("Gotten data: name='%s', enemy_name='%s', game_start='%s'\n", name, enemy_name, game_start);

    int state = HANDSHAKE;
    char buf[80]; memset(buf, 0, sizeof(buf));
    char data[2048]; memset(data, 0, sizeof(data));
    char recv_msg[80]; memset(recv_msg, 0, sizeof(recv_msg));

    while (state != CLOSED) {

        ssize_t bytes = recv(fd, buf, sizeof(buf), 0); // MSG_DONTWAIT is not set
        printf("%s", buf);
        if (bytes > 0) {
            while(assembleString(recv_msg, data, buf)) {
                state = client(state, fd, recv_msg);
                memset(recv_msg, 0, sizeof(recv_msg));
                memset(buf, 0, sizeof(buf));
            }
        }
        else {
            printf("Problems with recv or connection was closed\n");
            memset(recv_msg, 0, sizeof(recv_msg));
            memset(buf, 0, sizeof(buf));
            state = CLOSED;
        }
        memset(buf, 0, sizeof(buf));

    }

    close(fd);
}


bool assembleString(char* recv_msg, char* data, char* buf) {
    size_t strL1 = strlen(data);
    size_t strL2 = strlen(buf);

    for (size_t i = strL1; i < strL1 + strL2; i++) {
        data[i] = buf[i - strL1];
    }

    bool ok = false;
    if (data[strL1+strL2-1] == '\n') {
        size_t recv_len = 0;
        for (size_t i = 0; i < strL1 + strL2; i++) {
            if (i > 0 && data[i-1] == '\n')
                break;
            recv_len++;
            recv_msg[i] = data[i];
        }
        for (size_t i = recv_len; i < strlen(data); i++) {
            data[i - recv_len] = data[i];
        }
        size_t len = strlen(data);
        for (size_t i = len-recv_len; i < len; i++) {
            data[i] = '\0';
        }
        ok = true;
    }

    if (ok)
        return true;
    else
        return false;
}


int client(int state, int fd, char* recv_msg) {
    switch(state) {
        case HANDSHAKE:
            state = handshake(fd, recv_msg);
            break;
        case ACQ:
            state = acq(fd, name, recv_msg);
            break;
        case ACQ_CONFIRM:
            state = acqConfirm(recv_msg);
            break;
        case CHOOSE_RIVAL_GET_LIST:
            state = chooseRivalGetList(recv_msg);
            break;
        case CHOOSE_RIVAL_GET_NAMES:
            state = chooseRivalGetNames(recv_msg);
            break;
        case CHOOSE_RIVAL:
            state = chooseRival(fd, enemy_name, recv_msg);
            break;
        case CHOOSE_RIVAL_CONFIRM:
            state = chooseRivalConfirm(recv_msg);
            break;
        case WAIT_GAME_OFFER:
            state = waitGameOffer(fd, game_start, recv_msg);
            break;
        case WAIT_GAME_STATUS:
            state = waitGameStatus(recv_msg);
            break;
        case PLACE_SHIPS:
            state = placeShips(fd, recv_msg);
            break;
        case GAME_START:
            state = gameStart(recv_msg);
            break;
        case GAME_ACTION:
            state = gameAction(fd, recv_msg);
            break;
        case GET_FIELD:
            state = getField(fd, recv_msg);
            break;
    }

    return state;
}


int handshake(int fd, char* recv_msg) {
    char Rmsg[] = "battle ship server 1\n";
    char Smsg[] = "battle ship client 1\n";

    if (!strcmp(recv_msg, Rmsg)) {
        send(fd, Smsg, sizeof(Smsg), 0);
        return ACQ;
    }

    return CLOSED;
}


int acq(int fd, char* name, char* recv_msg) {
    char Rmsg[] = "name?\n";
    if (!strcmp(recv_msg, Rmsg)) {
        name = strcat(name, "\n");
        send(fd, name, strlen(name), 0);
        return ACQ_CONFIRM;
    }

    return CLOSED;
}


int acqConfirm(char* recv_msg) {
    char Rmsg1[] = "name: accepted\n";
    char Rmsg2[] = "name: rejected\n";

    if (!strcmp(Rmsg1, recv_msg)) {
//        printf("name: accepted\n");
        return CHOOSE_RIVAL_GET_LIST;

    } else if (!strcmp(Rmsg2, recv_msg)) {
//        printf("name: rejected\n");
        return 0;
    }

    return CLOSED;
}


int chooseRivalGetList(char* recv_msg) {
    char Rmsg1[] = "list";
    char Rmsg2[] = ":";
    // here we believe to the server that the protocol is correct and there is no more spaces in a string

    char msgR1[80];
    char msgR2[80];
    int num;

    sscanf(recv_msg, "%s %d%s", msgR1, &num, msgR2);

    if (!strcmp(Rmsg1, msgR1) && !strcmp(Rmsg2, msgR2)) {
        clients_cnt = num;
        if (num > 0)
            return CHOOSE_RIVAL_GET_NAMES;
        else
            return CHOOSE_RIVAL;
    }
    return CLOSED;
}


int chooseRivalGetNames(char* recv_msg) {

    if (clients_cnt > 1) {
        clients_cnt--;
        return CHOOSE_RIVAL_GET_NAMES;
    } else if (clients_cnt == 1) {

    } else {

    }

    return CHOOSE_RIVAL;
}


int chooseRival(int fd, char* enemy_name, char* recv_msg) {
    char Rmsg[] = "enemy?\n";

    if (!strcmp(Rmsg, recv_msg)) {
        enemy_name = strcat(enemy_name, "\n");
        send(fd, enemy_name, strlen(enemy_name), 0);
        return CHOOSE_RIVAL_CONFIRM;
    }

    return CLOSED;
}


int chooseRivalConfirm(char* recv_msg) {
    char Rmsg1[] = "enemy: ok\n";
    char Rmsg2[] = "enemy: rejected\n";

    if (!strcmp(Rmsg1, recv_msg)) {
//        printf("enemy: ok\n");
        return WAIT_GAME_OFFER;

    } else if (!strcmp(Rmsg2, recv_msg)) {
//        printf("enemy: rejected\n");
        return 0;
    }

    return CLOSED;
}


int waitGameOffer(int fd, char* game_start, char* recv_msg) {
    char msg[80];
    char enemy_nick[80];

    char Rmsg[] = "game:";
    sscanf(recv_msg, "%s %s\n", msg, enemy_nick);

    if (!strcmp(Rmsg, msg)) {
        game_start = strcat(game_start, "\n");
        send(fd, game_start, strlen(game_start), 0);
        return WAIT_GAME_STATUS;
    }
    printf("HERE\n");
    return CLOSED;
}


int waitGameStatus(char* recv_msg) {
    char msg[1000];
    memset(msg, 0, sizeof(msg));

    char Rmsg1[] = "invite: accepted\n";
    char Rmsg2[] = "invite: rejected\n";

    if (!strcmp(Rmsg1, recv_msg)) {
        return PLACE_SHIPS;
    }
    else if (!strcmp(Rmsg2, recv_msg)) {
        printf("INVITE WAS REJECTED\n");
        return CLOSED;
    }

    return CLOSED;
}


int placeShips(int fd, char* recv_msg) {
    char ships[10][6] = {
        "h:A0\n", "h:A2\n", "h:A4\n", "h:F0\n", "h:E2\n", "h:E4\n", "h:A6\n", "h:A8\n", "h:C9\n", "h:E9\n"
    };

    char Rmsg1[] = "place";
    char Rmsg2[] = "-ship?";

    char msgR1[80];
    char msgR2[80];
    int ship_len;

    if (ships_cnt > 1) {
        sscanf(recv_msg, "%s %d%s", msgR1, &ship_len, msgR2);
        if (!strcmp(Rmsg1, msgR1) && !strcmp(Rmsg2, msgR2) && 0 < ship_len && 4 >= ship_len) {
            send(fd, ships[10-ships_cnt], strlen(ships[10-ships_cnt]), 0);
            ships_cnt--;
            return PLACE_SHIPS;
        }
        return CLOSED;
    }
    else if (ships_cnt == 1) {
        sscanf(recv_msg, "%s %d%s", msgR1, &ship_len, msgR2);
        if (!strcmp(Rmsg1, msgR1) && !strcmp(Rmsg2, msgR2) && 0 < ship_len && 4 >= ship_len) {
            send(fd, ships[10-ships_cnt], strlen(ships[10-ships_cnt]), 0);
            ships_cnt--;
            return GAME_START;
        }
        return CLOSED;

    }
    else {

    }

    return GAME_START;
}


int gameStart(char* recv_msg) {
    char Rmsg[] = "go\n";

    if (!strcmp(Rmsg, recv_msg)) {
        return GAME_ACTION;
    }

    return CLOSED;
}


int gameAction(int fd, char* recv_msg) {
    if (!strcmp(recv_msg, "end: win\n") || !strcmp(recv_msg, "end: loss\n") || !strcmp(recv_msg, "end: techwin\n") || !strcmp(recv_msg, "end: techlose\n")) {
        return CLOSED;
    }
    else if (!strcmp(recv_msg, "fire?\n")) {
        char shot[80];

        if (flag) {
            flag = false;
            send(fd, "field\n", 6, 0);
            return GET_FIELD;
        }
        sleep(1);
        char pos1 = (char)(pos % 10+'A');
        char pos2 = (char)(pos / 10+'0');
        sprintf(shot, "%c%c\n", pos1, pos2);

        printf("%s", shot);
        pos++;
        send(fd, shot, strlen(shot), 0);
        return GAME_ACTION;
    }
    else {
        return GAME_ACTION;
    }
}


int getField(int fd, char* recv_msg) {
    if (!strcmp(recv_msg, "-+----------+  -+----------+\n")) {
        char shot[80];
        char pos1 = (char)(pos % 10+'A');
        char pos2 = (char)(pos / 10+'0');
        sprintf(shot, "%c%c\n", pos1, pos2);

        printf("%s", shot);
        pos++;
        send(fd, shot, strlen(shot), 0);

        return GAME_ACTION;
    }
    else {
        return GET_FIELD;
    }
}
